from fastapi import FastAPI, status, HTTPException
from sqlalchemy import *
from typing import Optional
from pydantic import BaseModel
import os
from uuid import uuid4
import sys

user = os.getenv('DB_USER')
password = os.getenv('DB_PASSWORD')
host = os.getenv('DB_HOST')
database = os.getenv('DB_DATABASE')

conn_str = f"postgresql://{user}:{password}@{host}/{database}"
engine = create_engine(conn_str)
connection = engine.connect()

app = FastAPI()

class PasswordAuth(BaseModel):
    inserted_pwd_hash: str



@app.get("/")
async def root():
    # Read
    return {'message':'Bienvenidos a Condo App'}

# Login
@app.post("/users/{item_id}")
async def authentication(item_id, PasswordAuth: PasswordAuth):
    # Read
    results = []
    result_set = engine.execute(f"SELECT password_hash FROM users WHERE username='{item_id}'")
    print(result_set)
    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="User Not Found")
    elif results[0]['password_hash'] == PasswordAuth.inserted_pwd_hash:
        rand_token = uuid4()
        engine.execute(f"INSERT INTO public.tokens (\"user\", token_string) VALUES ('{item_id}','{str(rand_token)}')")
        return {"detail":"Correct authentication","token":str(rand_token)}
    else:
        raise HTTPException(status_code=401, detail="Incorrect Password")

# GET user data
@app.get("/user_data/{token}")
async def get_user_data(token):
    # Read
    results = []
    result_set = engine.execute(f"SELECT \"user\" FROM tokens WHERE token_string='{token}'")
    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']
        print(user)
        results = []
        result_set = engine.execute(f'''SELECT u.username, u.first_name, u.second_name, u.birth_date, u.email, u.phone, u.document_id, u.document_type, r.residence_id, r.resident_id
            FROM users u
            inner join residents r on u.user_id = r.user_id
            WHERE username=\'{user}\'''')
        for i in result_set:
            results.append(i)
        return results[0]

# get residence info
@app.get("/residences/{token}/{residence_id}")
async def get_user_data(token,residence_id):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")
    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']
        print(user)
        results = []
        result_set = engine.execute(f'''select address, name, house_number, type, interior, additional_info
        from condominiums c
        inner join residences r on r.condo_id=c.condo_id
        WHERE residence_id={residence_id};''')
        for i in result_set:
            results.append(i)
        return results[0]

# get residence info
@app.get("/near_unpaid_payments/{token}")
async def get_user_data(token):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")
    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']
        print(user)
        results = []
        result_set = engine.execute(f'''SELECT p.*
        FROM users u
        inner join residents r on u.user_id = r.user_id
        inner join payments p on r.residence_id = p.residence_id
        WHERE username='{user}' and status='Unpaid' and due_date<CURRENT_DATE+3;''')
        for i in result_set:
            results.append(i)
        if(len(results)==0):
            raise HTTPException(status_code=200, detail="The user does not have any payments")
        else:
            return results

# get payments
@app.get("/payments/{token}")
async def get_user_data(token):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")
    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']
        print(user)
        results = []
        result_set = engine.execute(f'''SELECT p.*
        FROM users u
        inner join residents r on u.user_id = r.user_id
        inner join payments p on r.residence_id = p.residence_id
        WHERE username='{user}' 
        order by due_date asc;''')
        for i in result_set:
            results.append(i)
        if(len(results)==0):
            raise HTTPException(status_code=200, detail="The user does not have any payments")
        else:
            return results

# put auth times
@app.get("/authtimes/{token}/{platform}/{time}")
async def post_user_times(token,platform,time):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")
    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        if platform != 'android' and platform != 'ios':
            raise HTTPException(status_code=404, detail="Wrong Platform")
        else:
            engine.execute(f"INSERT INTO public.authentication_times (time, platform) VALUES ({float(time)}, '{platform}')")
            raise HTTPException(status_code=200, detail="Added correctly")

# put notifications times
@app.get("/notifications/{token}/{platform}/{req}")
async def post_notifications(token,platform,req):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")
    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        if platform != 'android' and platform != 'ios':
            raise HTTPException(status_code=404, detail="Wrong Platform")
        else:
            if platform == 'android':
                if req=='open':
                    engine.execute(f'''update notifications
                    set open_notification_people = open_notification_people + 1
                    where platform = 'android';''')
                    raise HTTPException(status_code=200, detail="Added correctly")
                if req=='camera':
                    engine.execute(f'''update notifications
                    set open_camera = open_camera + 1
                    where platform = 'android';''')
                    raise HTTPException(status_code=200, detail="Added correctly")
                if req=='notified':
                    engine.execute(f'''update notifications
                    set notified_people = notified_people + 1
                    where platform = 'android';''')
                if req=='rememberyes':
                    engine.execute(f'''update notifications
                    set remember_yes = remember_yes + 1
                    where platform = 'android';''')
                if req=='rememberno':
                    engine.execute(f'''update notifications
                    set remember_no = remember_no + 1
                    where platform = 'android';''')
                raise HTTPException(status_code=200, detail="Added correctly")
            if platform == 'ios':
                if req=='open':
                    engine.execute(f'''update notifications
                    set open_notification_people = open_notification_people + 1
                    where platform = 'ios';''')
                    raise HTTPException(status_code=200, detail="Added correctly")
                if req=='camera':
                    engine.execute(f'''update notifications
                    set open_camera = open_camera + 1
                    where platform = 'ios';''')
                    raise HTTPException(status_code=200, detail="Added correctly")
                if req=='notified':
                    engine.execute(f'''update notifications
                    set notified_people = notified_people + 1
                    where platform = 'ios';''')
                if req=='rememberyes':
                    engine.execute(f'''update notifications
                    set remember_yes = remember_yes + 1
                    where platform = 'ios';''')
                if req=='rememberno':
                    engine.execute(f'''update notifications
                    set remember_no = remember_no + 1
                    where platform = 'ios';''')
                    raise HTTPException(status_code=200, detail="Added correctly")

@app.get("/month_paid/{token}")
async def get_user_data(token):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")
    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']
        print(user)
        results = []
        result_set = engine.execute(f'''select sum(amount)
from payments p
inner join residents r on p.residence_id = r.residence_id
inner join users u on r.user_id = u.user_id
where extract(month from payment_date) = extract(month from current_date) and extract(year from payment_date) = extract(year from current_date) and status='Paid'
group by username, extract(month from payment_date), extract(year from payment_date), status
having username='{user}';''')
        for i in result_set:
            results.append(i)
        if(len(results)==0):
            raise HTTPException(status_code=200, detail="The user does not have any payments")
        else:
            return results

@app.get("/payments_ml_model/{token}/{month}")
async def get_user_data(token, month):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")

    if (not (int(month) >=1 and int(month) <=12)):
        raise HTTPException(status_code=404, detail="Wrong Month")
    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']

        result_set = engine.execute('''select residence_id, 
        extract(month from due_date) as due_month, 
        amount from payments
        order by due_month;''')

        id_residences = []
        month_number = []
        amount = []

        for i in result_set:
            id_residences.append(i[0])
            month_number.append(i[1])
            amount.append(i[2])

        import pandas as pd
        from sklearn import linear_model

        Stock_Market = {'Id_residences': id_residences,
                'Month': month_number,
                'Amount': amount      
                }

        df = pd.DataFrame(Stock_Market,columns=['Id_residences','Month','Amount'])

        print(df)

        X = df[['Id_residences','Month']]
        Y = df['Amount']

        regr = linear_model.LinearRegression()
        regr.fit(X, Y)

        query_residence = f'''select residence_id
        from residents
        inner join users u on residents.user_id = u.user_id
        where username='{user}';'''

        results = []
        residence_result = engine.execute(query_residence)
        for i in residence_result:
            results.append(i)

        residence = results[0][0]
        print(regr.predict([[residence,month]]))

        return {"prediction":str(regr.predict([[residence,month]])[0])}

@app.get("/visitors/frequent/{token}")
async def get_frequent_visitors(token):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")

    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']

        query_visitors = f'''select first_name, second_name, phone, document_type, document_id
        from users
        where user_id in (select condo_visitor.user_id visitor_id
        from condo_visitor
        inner join residents r2 on condo_visitor.residence_id = r2.residence_id
        inner join users u on r2.user_id = u.user_id
        where frequent = true and username = '{user}')'''

        result_set = engine.execute(query_visitors)

        results = []
        for i in result_set:
            results.append(i)
        if(len(results)==0):
            raise HTTPException(status_code=200, detail="The user does not frequent visitors")
        else:
            return results

@app.get("/visitors/nofrequent/{token}")
async def get_frequent_visitors(token):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")

    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']

        query_no_frequent = f'''select first_name, second_name, phone, document_type, document_id
                from users
                where user_id in (
                select visitor_id
                from visits
                inner join condo_visitor cv on visits.visitor_id = cv.user_id
                inner join residents r2 on cv.residence_id = r2.residence_id
                inner join users u on r2.user_id = u.user_id
                where (DATE_PART('year', visit_date::date) - DATE_PART('year', current_date::date)) * -12 +
              (DATE_PART('month', visit_date::date) - DATE_PART('month', current_date::date)) < 3 
              and cv.frequent = false and username = '{user}');'''

        result_set = engine.execute(query_no_frequent)

        results = []
        for i in result_set:
            results.append(i)
        if(len(results)==0):
            raise HTTPException(status_code=200, detail="The user does not frequent visitors")
        else:
            return results

@app.get("/visitors/last3months/{token}")
async def get_not_frequent_visitors_last_3_month(token):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")

    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']

        query_no_frequent = f'''select first_name, second_name, phone, document_type, document_id
                from users
                where user_id in (
                select visitor_id
                from visits
                inner join condo_visitor cv on visits.visitor_id = cv.user_id
                inner join residents r2 on cv.residence_id = r2.residence_id
                inner join users u on r2.user_id = u.user_id
                where (DATE_PART('year', visit_date::date) - DATE_PART('year', current_date::date)) * -12 +
              (DATE_PART('month', visit_date::date) - DATE_PART('month', current_date::date)) >= 3 
              and cv.frequent = false and username = '{user}');'''

        result_set = engine.execute(query_no_frequent)

        results = []
        for i in result_set:
            results.append(i)
        if(len(results)==0):
            raise HTTPException(status_code=200, detail="The user does not have not visits in the last 3 months")
        else:
            return results

class VisitorCreate(BaseModel):
    first_name: str
    last_name: str
    phone:str
    document_type:str
    document_number:str
    frequent:bool

# Login
@app.post("/visitor/{token}")
async def visitor_create(token, VisitorCreate: VisitorCreate):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")

    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']

        query_user = f'''
        INSERT INTO users (username, password_hash, role, first_name, second_name, birth_date, email, phone, document_type, document_id) VALUES (null, null, 'visitant', '{VisitorCreate.first_name}', '{VisitorCreate.last_name}', null, null, {int(VisitorCreate.phone)}, '{VisitorCreate.document_type}', '{VisitorCreate.document_number}');'''
        engine.execute(query_user)

        query_visitor = f'''INSERT INTO condo_visitor (user_id, frequent, residence_id) VALUES ((SELECT MAX(user_id) FROM users), {VisitorCreate.frequent}, (select r.residence_id
        from residents
        inner join residences r on residents.residence_id = r.residence_id
        inner join users u on residents.user_id = u.user_id
        where u.username = '{user}'));'''
        engine.execute(query_visitor)
        
        raise HTTPException(status_code=200, detail="Visitor created succesfully") 

class VisitCreate(BaseModel):
    first_name: str
    last_name: str
    parking:bool
    date:str
    time:str

@app.post("/visit/{token}")
async def visit_create(token, VisitCreate: VisitCreate):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")

    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']
        json_date = VisitCreate.date
        json_time = VisitCreate.time
        date = json_date[0:4] + "/" + json_date[4:6] + "/" + json_date[6:8]
        time = json_time[0:2] + ":" + json_time[2:4] + ":" + "00"
        final_datetime = date + " " + time
        query_visit = f'''
        INSERT INTO visits (visitor_id, visit_id, visit_date, parking) VALUES ((select condo_visitor.user_id
        from condo_visitor
        inner join users u on condo_visitor.user_id = u.user_id
        where residence_id = (select r.residence_id
        from residents
        inner join residences r on residents.residence_id = r.residence_id
        inner join users u on residents.user_id = u.user_id
        where u.username = '{user}') and first_name = '{VisitCreate.first_name}' and second_name = '{VisitCreate.last_name}'), DEFAULT, '{final_datetime}', {VisitCreate.parking});'''
        engine.execute(query_visit)
        
        raise HTTPException(status_code=200, detail="Visit created succesfully")   

# put cache amount
@app.get("/cacheamount/{token}/{platform}/{amount}")
async def cache_amount(token,platform,amount):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")
    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        if platform != 'android' and platform != 'ios':
            raise HTTPException(status_code=404, detail="Wrong Platform")
        else:
            engine.execute(f"INSERT INTO public.cache_amount (amount, platform) VALUES ({int(amount)}, '{platform}')")
            raise HTTPException(status_code=200, detail="Added correctly")     

# put online amount
@app.get("/online/{token}/{platform}/{amount}/{view}")
async def online_amount(token,platform,amount, view):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")
    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        if platform != 'android' and platform != 'ios':
            raise HTTPException(status_code=404, detail="Wrong Platform")
        if amount != '0' and amount != '1':
            raise HTTPException(status_code=404, detail="Wrong Value (0 false, 1 true)")
        else:
            engine.execute(f"INSERT INTO public.online_count (count, platform, view) VALUES ({int(amount)}, '{platform}', '{view}')")
            raise HTTPException(status_code=200, detail="Added correctly") 

@app.get("/services/{token}")
async def get_services(token):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")

    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']

        query_services = f'''
        select s.*
        from services s
        inner join condominiums c on s.condo_id=c.condo_id
        inner join residences r on c.condo_id = r.condo_id
        inner join residents res on r.residence_id=res.residence_id
        inner join users u on res.user_id = u.user_id
        where u.username = '{user}';
        '''
        result_set = engine.execute(query_services)

        results = []
        for i in result_set:
            results.append(i)
        if(len(results)==0):
            raise HTTPException(status_code=200, detail="The user does not frequent visitors")
        else:
            return results    

# Get code
@app.get("/create/resident/{code}")
async def create_resident_code(code):
    # Read
    results = []
    result_set = engine.execute(f"select * from signup_token where token='{code}';")

    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="This code does not exist.")
    else:
        print(results)
        used = results[0]['used']

        if used == True:
            raise HTTPException(status_code=404, detail="This code was used.")
        else:
            engine.execute(f"UPDATE signup_token SET used = true WHERE token='{code}';")
            raise HTTPException(status_code=200, detail="The token is correct.")

class CreateUser(BaseModel):
    username:str
    password_hash:str
    first_name:str
    last_name:str
    phone:str
    document_type:str
    document_id:str
    code:str


# Create resident
@app.post("/create/resident")
async def visitor_create(CreateUser: CreateUser):
    try:
        results = []
        max_list = engine.execute(f'select max(user_id) from users;')

        for i in max_list:
            results.append(i)
        
        max_user = results[0]['max'] + 1

        engine.execute(f'''
        INSERT INTO users (user_id, username, password_hash, role, first_name, second_name, phone, document_type, document_id)
        VALUES ({max_user}, '{CreateUser.username}', '{CreateUser.password_hash}', 'resident', 
        '{CreateUser.first_name}', '{CreateUser.last_name}', {int(CreateUser.phone)}, '{CreateUser.document_type}', 
        '{CreateUser.document_id}');
        ''')

        results = []
        max_list = engine.execute(f"select residence_id from signup_token where token = '{CreateUser.code}';")

        for i in max_list:
            results.append(i)
        
        residence_id = results[0][0]

        results = []
        max_list = engine.execute(f'select max(resident_id) from residents;')

        for i in max_list:
            results.append(i)

        max = results[0]['max'] + 1
        engine.execute(f'''
        INSERT INTO residents(resident_id, residence_id, user_id)
        VALUES ({max}, {residence_id}, {max_user});
        ''')
        
        raise HTTPException(status_code=200, detail="Added correctly.")
    except HTTPException as e:
        raise HTTPException(status_code=200, detail="Added correctly.")
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)


# put cache amount
@app.get("/register_time/{platform}/{amount}")
async def cache_amount(platform,amount):
    # Read
    if platform != 'android' and platform != 'ios':
            raise HTTPException(status_code=404, detail="Wrong Platform")
    else:
            engine.execute(f"INSERT INTO register_times (time, platform) VALUES ({float(amount)}, '{platform}')")
            raise HTTPException(status_code=200, detail="Added correctly") 



@app.get("/reservations/{name}/{date}")
async def get_reservations_schema(name, date):
    # Read
    results = []
    result_set = engine.execute(f'''select rs.* from reservation_schemas rs
    inner join services s on rs.service_id = s.service_id
    where s.name = '{name}' and DATE(rs.finish_date) = '{date}' and DATE(rs.start_date) = '{date}';''')

    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="No reservations for this day.")
    else:
        return results

@app.get("/user_id/{token}")
async def get_user_id(token):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")

    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']

        query_services = f'''
        select resident_id
        from residents
        inner join users u on residents.user_id = u.user_id
        where u.username = '{user}';
        '''
        result_set = engine.execute(query_services)

        results = []
        for i in result_set:
            results.append(i)
        if(len(results)==0):
            raise HTTPException(status_code=200, detail="The user does not exist")
        else:
            return results  

@app.get("/reservations/{token}/{service}/{date}/{hour}/{minute}")
async def get_user_id(token, service, date, hour, minute):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")

    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']

        try:
            query_services = f'''
            update reservation_schemas
            set resident_id = (select resident_id
            from residents
            inner join users u on residents.user_id = u.user_id
            where u.username = '{user}')
            where date(start_date) = '{date}' and extract(hour from start_date)={hour} and extract(minute from start_date)={minute}
            and service_id = (select service_id from services where name='{service}');;
            '''
            print(query_services)
            engine.execute(query_services)
            raise HTTPException(status_code=200, detail="Reserved correctly.") 
        except HTTPException:
            raise HTTPException(status_code=200, detail="Reserved correctly.") 
        except Exception as e:
            print(e)
            raise HTTPException(status_code=500, detail="Server Error") 

@app.get("/favorite/{token}")
async def get_favorite_service(token):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")

    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']

        query_services = f'''
            select name
            from reservation_schemas
            inner join services s on reservation_schemas.service_id = s.service_id
            group by resident_id, name
            having resident_id = (select resident_id
            from residents
            inner join users u on residents.user_id = u.user_id
            where u.username = '{user}')
            order by count(reservation_schemas.service_id) desc
            limit 1;
            '''
        result_set = engine.execute(query_services)

        results = []
        for i in result_set:
                results.append(i)
        if(len(results)==0):
                raise HTTPException(status_code=200, detail="The user has no reservations")
        else:
                return results[0]['name']


# Reservation Times
@app.get("/reservation_time/{platform}/{amount}")
async def reservation_time(platform,amount):
    # Read
    if platform != 'android' and platform != 'ios':
            raise HTTPException(status_code=404, detail="Wrong Platform")
    else:
            engine.execute(f"INSERT INTO reservation_times (time, platform) VALUES ({float(amount)}, '{platform}')")
            raise HTTPException(status_code=200, detail="Added correctly") 


# Services
@app.get("/not_used/{token}")
async def not_used_services(token):
    # Read
    results = []
    result_set = engine.execute(f"SELECT * FROM tokens WHERE token_string='{token}'")

    for i in result_set:
        results.append(i)

    if(len(results)==0):
        raise HTTPException(status_code=404, detail="Wrong Token")
    else:
        user = results[0]['user']

        query_services = f'''
            select a.name from (
            select s.name from reservation_schemas
            inner join services s on reservation_schemas.service_id = s.service_id
            where date(start_date) between current_date-15 and current_date and resident_id = (select resident_id
            from residents
            inner join users u on residents.user_id = u.user_id
            where u.username = '{user}')
            group by s.name) u
            right join (select name
            from services
            where condo_id = (select condo_id
            from residents
            inner join residences r on residents.residence_id = r.residence_id
            where resident_id = (select resident_id
            from residents
            inner join users u on residents.user_id = u.user_id
            where u.username = '{user}'))) a
            on u.name = a.name
            where u.name is null;
            '''
        result_set = engine.execute(query_services)

        results = []
        for i in result_set:
                results.append(i)
        if(len(results)==0):
                raise HTTPException(status_code=200, detail="The user used all the services.")
        else:
                return results


  